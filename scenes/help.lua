local composer = require( "composer" );
 
local scene = composer.newScene();
 
function scene:create( event )
    
    topBtnwidth = (8*display.contentWidth)/100;
    print(topBtnwidth);
    
    topBtnheight = (8*display.contentHeight)/100;
    print(topBtnheight);
    
 
     local sceneGroup = self.view
    --  back=display.newImage("UIcomponents/music.png");
    local home=display.newImage("UIcomponents/home.png");
     local background = display.newImage("UIcomponents/background.png");
     background.x = display.contentCenterX;
     background.y = display.contentCenterY;
     background.width = display.contentWidth;
     background.height = display.contentHeight;
     sceneGroup:insert (background);
     background:toBack();
     print (background)
    
     local helpText = "HELP";
     local helpText = display.newText( helpText, display.contentCenterX, 65, native.systemFont, 25);
     sceneGroup:insert (helpText);
    



    -- music.width=topBtnwidth;
    -- music.height=topBtnheight;
    
    home.x=45;
    home.y=65;

    home.width=topBtnwidth;
    home.height=topBtnwidth;
    
    -- sceneGroup:insert(music);
    sceneGroup:insert(home);

        
     local rectBorder = display.newRect (display.contentCenterX, 350, 320, 500);
     rectBorder.strokeWidth = 1.5;
     rectBorder:setStrokeColor(255,255,255);
     rectBorder:setFillColor(gray, 0);
     sceneGroup:insert (rectBorder);
    
     local helpTitle = "HOW TO PLAY";
     local howtoplayText = display.newText( helpTitle, display.contentCenterX, 140, native.systemFont, 20);
     sceneGroup:insert (howtoplayText);
     howtoplayText:toFront();
    
     local howtoplayDash = display.newRoundedRect( display.contentCenterX,165, 150, 10, 10)
     howtoplayDash:setFillColor(255,255,0);
     sceneGroup:insert (howtoplayDash);
    
    
     local backArrow = display.newImage("UIcomponents/back.png");
     backArrow.x = 40;
     backArrow.y = display.contentCenterY;
     backArrow.width = 20;
     backArrow.height = 20;
     backArrow.val = -1;
     backArrow.alpha=0;
     sceneGroup:insert (backArrow);
    
     local nextArrow = display.newImage("UIcomponents/next.png");
     nextArrow.x = 320;
     nextArrow.y = display.contentCenterY;
     nextArrow.width = 20;
     nextArrow.height = 20;
     nextArrow.val = 1;
     
     sceneGroup:insert (nextArrow);

     
 
    
     
    
     local myCircle1 = display.newCircle(160, 570, 3.5)
     local myCircle2 = display.newCircle(190, 570, 3.5)
     local myCircle3 = display.newCircle(220, 570, 3.5)
     myCircle1:setFillColor( 255,255,255 );
     myCircle2:setFillColor( 255,255,255 );
     myCircle3:setFillColor( 255,255,255 );
     sceneGroup:insert (myCircle1);
     sceneGroup:insert (myCircle2);
     sceneGroup:insert (myCircle3);
    
     local sliderDash = display.newRoundedRect(160, 570, 30, 10, 10)
     sliderDash:setFillColor(255,255,0);
     sceneGroup:insert (sliderDash);
     sliderDash:toFront();
    
    local cirleArray = { {50, 180}, {200, 260}, {280, 340}, {150, 400}};
    local circleColor = { {255,0,0}, {255,255,0}, {0,0,255}, {0, 255, 0}};
    
     for count=1, 4 do
        local circleFill = display.newCircle(cirleArray[count][1],cirleArray[count][2], 15);
        circleFill:setFillColor(unpack(circleColor[count])); 
        sceneGroup:insert (circleFill);
    end
    
    
    
    local currentSlide = 1;
    local slidePosition = display.contentWidth
    local page1Group = display.newGroup()
    local helpText1 = {text = "1. The balls with different colors will randomly fall.",     
    x = display.contentCenterX,
    y = 500,
    width = 300,
    font = native.systemFont,   
    fontSize = 18,
    align = "center"}
    local createhelpText1 = display.newText(helpText1);
    page1Group:insert (createhelpText1);
    
    local page2Group = display.newGroup()
    local helpText2 = {text = "2. The color bar below can be moved from right to left and can be used to collect the color balls which matches the bar color to earn the points.",     
    x = display.contentCenterX + display.contentWidth,
    y = 500,
    width = 300,
    font = native.systemFont,   
    fontSize = 18,
    align = "center"}
    local createhelpText2 = display.newText(helpText2);
    page2Group:insert (createhelpText2);
    page2Group.alpha=0;
    
    
    local page3Group = display.newGroup()
    local helpText3 = {text = "3. Test",     
    x = display.contentCenterX + display.contentWidth*2,
    y = 500,
    width = 300,
    font = native.systemFont,   
    fontSize = 18,
    align = "center"};
    local createhelpText3 = display.newText(helpText3);
    page3Group:insert (createhelpText3);
    page3Group.alpha=0;
    
    local pagesGroup = display.newGroup()
    pagesGroup:insert (page1Group);
    pagesGroup:insert (page2Group);
    pagesGroup:insert (page3Group);
    pagesGroup.x = 0
    
    sceneGroup:insert (pagesGroup);
    local function changeSlide(event)
        print(page1Group);
        print(pagesGroup[1]);
        
        print(event.target.val)
        
        if currentSlide <= 3 and currentSlide >=1 then
            nextArrow.alpha=1;
            backArrow.alpha=1;

        
    currentSlide=currentSlide+event.target.val
    print(currentSlide);
     --[[pagesGroup.x = -(slidePosition*currentSlide)]]
            transition.to(pagesGroup, {time = 400, x= -(slidePosition*(currentSlide-1)),transition=easing.inOutCubic});
            transition.to(pagesGroup[currentSlide-event.target.val], {time = 200, alpha= 0,transition=easing.inOutCubic});
            transition.to(pagesGroup[currentSlide], {time = 1000, alpha= 1,transition=easing.inOutCubic});
            
            
            
        if currentSlide ==3 then
          nextArrow.alpha=0;
        
        end 
            
            
        if currentSlide ==1 then
         backArrow.alpha=0;

        
        end 
                
        transition.to(sliderDash, {time = 400, x = sliderDash.x+(event.target.val*30),transition=easing.inOutCubic});
    
        end
    
    end



    local function gotohome(event)
 composer.gotoScene("scenes.gameoptions");


    end;

        


    nextArrow:addEventListener("tap", changeSlide)
    backArrow:addEventListener("tap", changeSlide)
    home:addEventListener("tap", gotohome)
           
        
end;


function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
        composer.removeScene("help");
 
    elseif ( phase == "did" ) then
        
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
  
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end





   

scene:addEventListener("create", scene);
scene:addEventListener( "show", scene );
scene:addEventListener( "hide", scene );
scene:addEventListener( "destroy", scene );

return scene;