local composer = require( "composer" )
 
local scene = composer.newScene();




local function replaygame(event)
    print("retry function called !");
    composer.gotoScene("scenes.game");
end;

function scene:create( event )

    local sceneGroup=self.view;
    local score = event.params.score;
    
    topBtnwidth = (18*display.contentWidth)/100;
    print(topBtnwidth);
    
    topBtnheight = (10*display.contentHeight)/100;
    print(topBtnheight);
    
    retrybuttonWidth=(23*display.contentWidth)/100;
    print(retrybuttonWidth);
    
    retrybuttonHeight=(13*display.contentHeight)/100;
    print(retrybuttonHeight);
    

background=display.newImage("UIcomponents/background.png", display.contentWidth, display.contentHeight);
logo=display.newImage("UIcomponents/logo.png");
local myText = display.newText( "You Scored", 0, 0, "cera.otf", 29 )
menu=display.newImage("UIcomponents/menu.png");

local myScore = display.newText( score, 0, 0, "cera.otf", 100 )
local retrybutton = display.newImage( "UIcomponents/retry.png");
arrow = display.newImageRect( "UIcomponents/arrow.png", display.contentWidth , 50);

local roundedRect = display.newRoundedRect( display.contentCenterX, 585, 130, 10, 10 )


 

menu.width=topBtnwidth;
menu.height=topBtnheight;  
myText.x = display.contentCenterX; myText.y = 240
myText:setFillColor( 1, 1, 1 )
myScore:setFillColor( 1, 1, 1 )
retrybutton.width=retrybuttonWidth;
retrybutton.height=retrybuttonHeight;
roundedRect:setFillColor( 255,255,0 )
    


myText.rotation = -11
print( myText.rotation )     
    
myScore.rotation = 2
print( myScore.rotation ) 

arrow.rotation = -12
print(arrow.rotation)
    
--roundedRect.rotation=4
--print(roundedRect.rotation)    
    

    --[[OBJECT SETTING]]
    
logo.x=display.contentCenterX;
logo.y=140;
background.x=display.contentCenterX;
background.y=display.contentCenterY;    
menu.x=45;
menu.y=55;
myScore.x=display.contentCenterX;
myScore.y=300
retrybutton.x=display.contentCenterX;
retrybutton.y=445
arrow.x=display.contentCenterX;
arrow.y=display.contentHeight-80;    

retrybutton:addEventListener( "tap", replaygame );
    
    
logo:scale(0.35,0.35);
    
sceneGroup:insert(background);
sceneGroup:insert(logo);
sceneGroup:insert(myScore);
sceneGroup:insert(retrybutton);
sceneGroup:insert(myText);
sceneGroup:insert(menu);
sceneGroup:insert(arrow);
sceneGroup:insert(roundedRect);




end;
   

function scene:enter( event )
    
end;
function scene:exitScene( event )

end;

    function scene:hide( event )
        local sceneGroup = self.view;
        local phase = event.phase;
     
        if ( phase == "will" ) then
            -- Code here runs when the scene is on screen (but is about to go off screen)
            composer.removeScene("help");
     
        elseif ( phase == "did" ) then
            
            -- Code here runs immediately after the scene goes entirely off screen
     
        end;
    end;


    function scene:destroyScene( event )
        local sceneGroup = self.view
        -- composer.removeScene("scenes.scored"); 
    end;


scene:addEventListener("create", scene);
scene:addEventListener( "enter", scene )
scene:addEventListener( "exitScene", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroyScene", scene )


return scene;