-- local composer = require("composer");
-- local scene = composer.newScene("gameoptions");

-- local background = display.newImageRect( "UIcomponents/background.png", display.contentWidth, display.contentHeight);
-- local arrow = display.newImageRect( "UIcomponents/arrow.png", display.contentWidth , 50);






-- print(scene.x);








-- -- background.x=display.contentCenterX;
-- -- background.y=display.contentCenterY;
-- -- background.alpha=1;

-- -- arrow.x=display.contentCenterX;
-- -- arrow.y=display.contentHeight-80;


-- function scene:create( event )
--     local group = self.view;
--     group:insert ( background )
-- end

-- function scene:enter( event )

-- end

-- function scene:exitScene( event )

-- end

-- function scene:destroyScene( event )

-- end

-- scene:addEventListener( "create", scene )
-- scene:addEventListener( "enter", scene )
-- scene:addEventListener( "exitScene", scene )
-- scene:addEventListener( "destroyScene", scene )

-- return scene



local composer = require( "composer" )
 
local scene = composer.newScene()
 
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
 
 
 
 -- -----------------------------------------------------------------------------------
-- Help event functions
-- -----------------------------------------------------------------------------------
 
-- create()
local function openHelp()
composer.gotoScene("scenes.help");
end;


 -- -----------------------------------------------------------------------------------
-- Scored event functions
-- -----------------------------------------------------------------------------------
 
-- create()
local function openscored()
composer.gotoScene("scenes.scored");
end;


-- -----------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------

-- -----------------------------------------------------------------------------------
-- Leaderboard event functions
-- -----------------------------------------------------------------------------------
 
-- create()
local function openleaderboard()
composer.gotoScene("scenes.leaderboard");
end    ;

-- -----------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------

function scene:create( event )
 

    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
     background = display.newImage( "UIcomponents/background.png", display.contentWidth, display.contentHeight);

    topBtnwidth = (18*display.contentWidth)/100;
    print(topBtnwidth);
    
    topBtnheight = (10*display.contentHeight)/100;
    print(topBtnheight);
    
    playBtnheight= (18*display.contentHeight)/100;
    print(playBtnheight);
    
    playBtnwidth= (30*display.contentWidth)/100;
    print(playBtnwidth);
 
    leaderboardBtnHeight=(13*display.contentHeight)/100;
    print(leaderboardBtnHeight);
    
    leaderboardbtnWidth=(22*display.contentWidth)/100;
    print(leaderboardbtnWidth);
    
    
     arrow = display.newImageRect( "UIcomponents/arrow.png", display.contentWidth , 50);
     local playbutton = display.newImage( "UIcomponents/mainplay.png",playBtnheight,playBtnwidth);
     logo=display.newImage("UIcomponents/logo.png");
     ranking=display.newImage("UIcomponents/rankings.png",leaderboardBtnHeight,leaderboardbtnWidth);
     trophy=display.newImage("UIcomponents/trophy.png",leaderboardBtnHeight,leaderboardbtnWidth);
     help=display.newImage("UIcomponents/help.png",topBtnheight,topBtnwidth);
     music=display.newImage("UIcomponents/music.png", topBtnheight,topBtnwidth);

    
    
    
   
    
    trophy.width=leaderboardbtnWidth;
    trophy.height=leaderboardBtnHeight;
    
    
    
    ranking.width=leaderboardbtnWidth;
    ranking.height=leaderboardBtnHeight;
    
    
    help.width=topBtnwidth;
    help.height=topBtnheight;
    
    music.width=topBtnwidth;
    music.height=topBtnheight;
    
    
    playbutton.width=playBtnwidth;
    playbutton.height=playBtnheight;
    
    
    
     background.x=display.contentCenterX;
     background.y=display.contentCenterY;
     background.alpha=1;
     arrow.x=display.contentCenterX;
     arrow.y=display.contentHeight-80;
     playbutton.x=display.contentCenterX;
     playbutton.y=display.contentCenterY;
     logo.x=display.contentCenterX;
     logo.y=display.contentCenterY-120;
     ranking.x=display.contentCenterX-90;
     ranking.y=display.contentCenterY+80;
     trophy.x=display.contentCenterX+90;
     trophy.y=display.contentCenterY+80;
     help.x=display.contentWidth-45;
     help.y=55;
     music.x=45;
     music.y=55;
    logo:scale(0.35,0.35);

    sceneGroup:insert(background);
    sceneGroup:insert(arrow);
    sceneGroup:insert(playbutton);
    sceneGroup:insert(trophy);
    sceneGroup:insert(help);
    sceneGroup:insert(music);
    sceneGroup:insert(ranking);
    sceneGroup:insert(logo);

    ----------------------------------------------------------------------------------------------
    --PLAY SCENE FUNCTIONS
    ----------------------------------------------------------------------------------------------
    local opengame = function()
        composer.gotoScene("scenes.game");
    end

    --------------------------------------------------------------------------------------
    --BUTTON EVENT LISTENERS
    --------------------------------------------------------------------------------------
    playbutton:addEventListener( "tap", opengame );
    --ranking:addEventListener("tap", openleaderboard);
    ranking:addEventListener("tap", openscored);
    help:addEventListener("tap", openHelp);
    --help:addEventListener("tap", openscored);
    
    
    end



 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
        composer.removeScene("gameoptions");
 
    elseif ( phase == "did" ) then
        
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
    print("__________JYOTI___________pandey__________");
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end


 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
--------------------------------------------------------------------------------------
-- EVENT LISTENERS
--------------------------------------------------------------------------------------



    
 
return scene


 