-- ONESIGNAL SETTINGS
-- This function gets called when the user opens a notification or one is received when the app is open and active.
-- Change the code below to fit your app's needs.
function DidReceiveRemoteNotification(message, additionalData, isActive)
    if (additionalData) then
        if (additionalData.discount) then
            native.showAlert( "Discount!", message, { "OK" } )
            -- Take user to your app store
        elseif (additionalData.actionSelected) then -- Interactive notification button pressed
            native.showAlert("Button Pressed!", "ButtonID:" .. additionalData.actionSelected, { "OK"} )
        end
    else
        native.showAlert("OneSignal Message", message, { "OK" } )
    end
end


function IdsAvailable(userID, pushToken)
    -- print("PLAYER_ID:" .. userID)
    if (pushToken) then -- nil if user did not accept push notifications on iOS
        -- print("PUSH_TOKEN:" .. pushToken)
    end

end


local OneSignal = require("plugin.OneSignal")
OneSignal.Init("2295f3fc-27a7-4426-9d6e-aa6781b981d7", "colordash-d5702", DidReceiveRemoteNotification);
OneSignal.IdsAvailableCallback(IdsAvailable);

-------------------------------------------------------------------------------------------------------
-- INITIALIZATION
-------------------------------------------------------------------------------------------------------

local composer = require("composer");
local scene = composer.newScene();



-- FUNCTUIONS

local function removephysics()
    physics.stop();
end

local function endgame()
    timer.cancel(timerfnball);
    timer.cancel(timerfndash);
    timerfnball = nil;  
    timerfndash = nil ;
    nextcolortransition=nil;
    gameended = true;
    timer.performWithDelay( 0, removephysics ); 
    --db.insertrecord(scorevalue);
    vent:stop();
    local options = { effect = "crossFade", time = 500, params = {score=scorevalue} }
    composer.gotoScene("scenes.scored",options);
end

local function indexof(table, value)
    for k,v in pairs(table) do
        if(v == value) then
            return k;
        end
    end
    return -1;
end

local function ballhit(self,event)
    print("BALL HIT",event.target.y.."  "..event.target.x);


    if(indexof(colorindextable, event.other.colorindex)~=-1) then 
       
        if event.phase == "began" then 
            -- vent:start();
            -- vent.emitX = event.target.x;
            -- vent.emitY =  event.target.y-15;
            emitter:start();
            emitter.x=  event.target.x;
            emitter.y =  event.target.y-15;
        
        end;
        
        scorevalue=scorevalue+1;
        event.other:removeSelf();
        -- vent:stop();
        -- print(event.other.y);
        score.text=tostring(scorevalue);
    else
        if (gameended == false) then
            endgame();
        end
    end
end

local function floorhit(self,event)
    -- if(dash.colorindex~=event.other.colorindex and event.other~=dash) then 
        event.other:removeSelf();
        -- else
        --     if (gameended == false) then
        --         endgame();
        --     end
        -- end

end


local function spawnball()
   local xpos=math.random(5,display.contentWidth-5);
   local ball=display.newCircle(xpos,0,15);
   if (balltable[spawnballcallcount]=='c') then
   ball:setFillColor(unpack(colorarray[dash.colorindex]));
   ball.colorindex=dash.colorindex;
   else
   local colorindex=math.random(1,3);
    ball:setFillColor(unpack(colorarray[colorindex]));
    ball.colorindex=colorindex;
   end;
   gamegroup:insert ( ball );
   ball.gravityScale=gravityscale;
    physics.addBody(ball,'dynamic');
--    print(ballspawntime);
-- ball.gravityScale=ball.gravityScale+0.2;
   spawnballcallcount=spawnballcallcount+1;
    end;

function dashhit(event) 
    if(gamepause==false and event.phase=="moved") then
    if(event.x>=35.7 and event.x<=display.contentWidth-35.7) then
        dash.x=event.x;
    end
    end

-- print(event.x,event.y);


end

function delayfunction()
table.remove(colorindextable,1);
dash:setFillColor(unpack(colorarray[dash.colorindex]));

end;


------------------------------------------------------------ POPUP DISMISS EVENT -----------------------------------------------------------

local function onComplete( event )
    if ( event.action == "clicked" ) then
        local i = event.index
        if ( i == 1 ) then
            composer.gotoScene('scenes.gameoptions');
            -- Do nothing; dialog will simply dismiss
        elseif ( i == 2 ) then
            composer.removeScene('scenes.game');
        composer.gotoScene('scenes.game');
        end;
    end;
end;
-- -----------------------------------------------------------SET RANDOM NUMBERS TO GET BALLS-------------------------------------------------
local function getcertainnumberofballs()

for num=1,numberofballs do
    if (num<=difference+counter) then 
    table.insert(balltable, 'c');
    else
        table.insert(balltable, 'uc');
    end
end;

for i = 1,numberofballs*2 do -- repeat this for twice the amount of elements in the table, to make sure everything is shuffled well
    local a = math.random(numberofballs);
    local b = math.random(numberofballs);
    balltable[a],balltable[b] = balltable[b],balltable[a];
    end;

end;


function changedash()
    -- dashwidth=0;
 
        -- timer.performWithDelay(500,function()
        --      dash.colorindex=colorindexfordash;
           
        -- end,1);
        -- timerdashcolorindicator._delay=45;    
        --   else
    --     dash.colorindex=colorindexfordash;
    --     table.insert(colorindextable, dash.colorindex);
    --     -- dash:setFillColor(unpack(colorarray[colorindexfordash]));

   
        
    --     -- dashcolorindicator=display.newRoundedRect(display.contentCenterX-(display.contentWidth/4),42.5,display.contentWidth/2,15,3);
        

        
    --     dashcolorindicator:setFillColor(unpack(colorarray[colorindexfordash]));
    --     dashcolorindicator.anchorX = 0;
    --     dashcolorindicator.width = display.contentWidth/2;
    --     print("FILL", dash.fill);
    --     print("FILL", dashcolorindicator.width);

       
        
      



    --     colorindexfordash=math.random(1,3);
    --    if(dash.colorindex==colorindexfordash) then 
    --         if(table.maxn(colorarray)==colorindexfordash) then
    --         colorindexfordash=1;
    --         else 
    --             colorindexfordash=colorindexfordash+1;
    --         end
    --     end
    --     nextdash:setFillColor(unpack(colorarray[colorindexfordash]));
    --     print("FILL", nextdash.fill);
       
    --     -- dashcolorindicator:toFront();
    --     if(table.maxn (colorindextable)>1) then
    --         print("table length :",table.maxn (colorindextable));
    --         dash:setFillColor(1,1,1);
    --         timerdelay=timer.performWithDelay(3000,delayfunction);
    --     else
    --         nextcolortransition = transition.to( dashcolorindicator, {width=0, time=5000,transition=linear });
    --     end


    --     -- gamegroup:insert ( dashcolorindicator );
    --     -- gamegroup:insert ( nextdash );


  if(targetlevel<=scorevalue or targetlevel<=10) then
        dash.colorindex=colorindexfordash;
        dashcolorindicator:setFillColor(unpack(colorarray[colorindexfordash]));
        table.insert(colorindextable, dash.colorindex);
        dashcolorindicator.anchorX=0;
        dashcolorindicator.width=display.contentWidth/2;
        nextcolortransition = transition.to( dashcolorindicator, {width=0, time=dashchangetime,transition=linear });

-- delay=1500,









     colorindexfordash=math.random(1,3);
       if(dash.colorindex==colorindexfordash) then 
            if(table.maxn(colorarray)==colorindexfordash) then
            colorindexfordash=1;
            else 
                colorindexfordash=colorindexfordash+1;
            end
        end
       





        if(table.maxn (colorindextable)>1) then
                    -- print("table length :",table.maxn (colorindextable));
                    dash:setFillColor(1,1,1);
                    timerdelay=timer.performWithDelay(1500,delayfunction);
                    if(decidingvalues-0.1>0) then
                    decidingvalues=decidingvalues-0.1;
                   
                end;
                difference=difference+5;


                gravityscale=gravityscale+0.2;

        else if(table.maxn (colorindextable)==1) then
            dash:setFillColor(unpack(colorarray[dash.colorindex]));
            gravityscale=1;
        end;
                end;


               
              
                
                nextdash:setFillColor(unpack(colorarray[colorindexfordash]));
                targetlevel=targetlevel+difference;
                numberofballs=50;
                -- print("number of balls",numberofballs);
                ballspawntime =math.round( dashchangetime/numberofballs);
                -- print("ballspawntime-",ballspawntime);
                targetscore.text='Target Score-:'..tostring(targetlevel);
                getcertainnumberofballs();
                -- dashchangetime=dashchangetime-10000;
                
            else 
                -- Show alert with two buttons
                -- physics.stop();
            endgame();
                -- local alert = native.showAlert( "You Lost !", "I am sorry you could not achieve given target.", { "End Game","Restart" }, onComplete )
                
                -- print("You have not achieved your target !");
        
            end;        
                                                                                                                                                                                                                                                                          
end;


local function pausegame(event)
    timer.pause(timerfnball);
    timer.pause(timerfndash);
    transition.pause(nextcolortransition);
    resumebutton.isVisible=true;
    pausebutton.isVisible=false;
    -- print(dash.x);
    -- pauses physics
    physics.pause();
    gamepause=true;

    end


    local function resumegame(event)
        timer.resume(timerfnball);
        timer.resume(timerfndash);
        transition.resume(nextcolortransition);
        resumebutton.isVisible=false;
        pausebutton.isVisible=true;
        -- pauses physics
        physics.start();
        gamepause=false;
    
    end



        -- local function networkListener( event )
 
        --     if ( event.isError ) then
        --         print( "Network error: ", event.response )
        --     else
        --         print ( "RESPONSE: " .. event.response )
        --     end
        -- end
          
        -- -- Access Google over SSL:
        -- network.request( "http://pixoloproductions.com/billmate/bmb/rest/index.php/tracking/getoneby?field=invoice_id&value=75", "GET", networkListener )




-- END OF FUNCTIONS



function scene:create( event )
    gamegroup = self.view;
    
    db=require('db');
    physics=require('physics');




-- EMITTER CODE
 emitterParams = {
    startColorAlpha = 1,
    startParticleSizeVariance = 53.47,
    startColorGreen = 0.3031555,
    yCoordFlipped = -1,
    blendFuncSource = 770,
    rotatePerSecondVariance = 153.95,
    particleLifespan = 0.7237,
    tangentialAcceleration = -144.74,
    finishColorBlue = 0.3699196,
    finishColorGreen = 0.5443883,
    blendFuncDestination = 1,
    startParticleSize = 50.95,
    startColorRed = 0.8373094,
    textureFileName = "UIcomponents/effect.png",
    startColorVarianceAlpha = 1,
    maxParticles = 256,
    finishParticleSize = 64,
    duration = -1,
    finishColorRed = 1,
    maxRadiusVariance = 72.63,
    finishParticleSizeVariance = 64,
    gravityy = -671.05,
    speedVariance = 90.79,
    tangentialAccelVariance = -92.11,
    angleVariance = -142.62,
    angle = -244.11
}











    --INITIALIZATIONS
    dashchangetime = 60000;
   
    gameended = false;
    colorindextable = {};
    

    --OBJECTS

   
    emitter = display.newEmitter( emitterParams );
    
    background = display.newImageRect( "UIcomponents/background.png", display.contentWidth, display.contentHeight);
    arrow = display.newImageRect( "UIcomponents/arrow.png", display.contentWidth , 50);
    colorarray={{0,0,1},{1,0,0},{0,1,0},{1,0,1}};
    pausebutton =display.newImageRect( "UIcomponents/pause.png",  (display.contentWidth/4), 65);
    resumebutton =display.newImageRect( "UIcomponents/resume.png",  (display.contentWidth/4), 65);
    dash=display.newRoundedRect(display.contentCenterX,display.contentHeight-110,75,15,10);
    floor=display.newRect(display.contentWidth/2,display.contentHeight+1,display.contentWidth,1);
    scorevalue=0;
    gamepause=false;
    nextdash=display.newRoundedRect(display.contentCenterX-(display.contentWidth/4),42.5,display.contentWidth/2,15,3);
    dashcolorindicator=display.newRoundedRect(display.contentCenterX-(display.contentWidth/4),47.5,display.contentWidth/2,15,3);
    balltable={};
    spawnballcallcount=1;
  
    -- print(dashcolorindicator);
    score=display.newText("0",(display.contentWidth/8)+5,50,display.contentWidth/4,65,native.systemFont,40);
    score:setFillColor(1,1,1);

    dashwidth=0;
    -- 
    CBE=require("CBE.CBE");
    CBE.listPresets();

    vent = CBE.newVent({
        preset = "sparks"
    });



    -- OBJECT SETTINGS
    background.x=display.contentCenterX;
    background.y=display.contentCenterY;
    background.alpha=1;
    floor.alpha=0;
    arrow.x=display.contentCenterX;
    arrow.y=display.contentHeight-80;
    pausebutton.x=display.contentWidth-(display.contentWidth/8);
    pausebutton.y=45;
    resumebutton.x=display.contentWidth-(display.contentWidth/8);
    resumebutton.y=45;
    resumebutton.isVisible=false;
    decidingvalues=dashchangetime/4000;
    difference=10;
    targetlevel=10;
    counter=10;
    numberofballs=50;
    ballspawntime=0;
    targetscore=display.newText("Target Score -:"..targetlevel+difference,(display.contentWidth/8)+5,90,display.contentWidth/4,70,'CeraGR.otf',18);

    nextdash.anchorX = 0;

    colorindexfordash=math.random(1,3);
    --GIVE DASH ITS INITIAL COLOR
    changedash();
    addbuffertime=true;

    timerfnball=timer.performWithDelay(ballspawntime,spawnball,-1);
    timerfndash=timer.performWithDelay(dashchangetime,changedash,-1);
    
    gamegroup:insert ( background );
    gamegroup:insert ( arrow );
    gamegroup:insert ( pausebutton );
    gamegroup:insert ( resumebutton );
    gamegroup:insert ( dash );
    gamegroup:insert ( floor );
    gamegroup:insert ( score );
    gamegroup:insert ( nextdash );
    gamegroup:insert ( dashcolorindicator );
    gamegroup:insert(targetscore);
    

    -- PHYSICS
    physics.start();
    physics.addBody(floor,'static');
    physics.addBody(dash,'static');
    dash.collision=ballhit;
    floor.collision=floorhit;


    dash:addEventListener("collision");
    floor:addEventListener("collision");
    pausebutton:addEventListener( "tap", pausegame );
    resumebutton:addEventListener( "tap", resumegame );
    Runtime:addEventListener('touch',dashhit);

end

function scene:enter( event )

end

function scene:exitScene( event )

end

-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
        -- print("--------------------hide--------------------")
      
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
       
        composer.removeHidden();
 
    end
end

function scene:destroyScene( event )
    
    composer.removeScene("game"); 
end


scene:addEventListener( "create", scene )
scene:addEventListener( "enter", scene )
scene:addEventListener( "exitScene", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroyScene", scene )

return scene



   

-- local particleDesigner = require( "particleDesigner" );
-- local emitter = particleDesigner.newEmitter( "nebula.json" );
-- emitter.x = display.contentCenterX;
-- emitter.y = display.contentCenterY;





